# AZRS tracking

Projekat iz AZRS za 2023/24 godinu.

Zadatak je primena alata na projektu iz RS koji je radjen u skolskoj 2022/23 godini. Ime RS projekta je "SNAP - SNAP is Not Another Photoshop".  
Ideja SNAP-a je da omogucava edit-ovanje slika raznim transformacijama (scale, merge, crop...).  
[Link ka originalnom SNAP repozitorijumu](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2022-2023/14-snap-snap-is-not-another-photoshop)

Korisceni alati:

1. [cmake](https://gitlab.com/veksku/azrs-tracking/-/issues/1)
2. [gdb](https://gitlab.com/veksku/azrs-tracking/-/issues/2)
3. [git](https://gitlab.com/veksku/azrs-tracking/-/issues/3)
4. [valgrind](https://gitlab.com/veksku/azrs-tracking/-/issues/4)
5. [clang-tidy](https://gitlab.com/veksku/azrs-tracking/-/issues/5)
6. [clang-format](https://gitlab.com/veksku/azrs-tracking/-/issues/6)
7. [docker](https://gitlab.com/veksku/azrs-tracking/-/issues/7)
8. [staticka analiza koda](https://gitlab.com/veksku/azrs-tracking/-/issues/8)
9. [git-hook](https://gitlab.com/veksku/azrs-tracking/-/issues/9)